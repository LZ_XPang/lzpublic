//
//  LoginViewController.m
//  YuetTianyun
//
//  Created by glen on 16/4/5.
//  Copyright © 2016年 glen. All rights reserved.
//

#import "LoginViewController.h"
#import <WebKit/WebKit.h>
#import <JavaScriptCore/JavaScriptCore.h>
@interface LoginViewController ()<
WKNavigationDelegate,
WKUIDelegate,
WKScriptMessageHandler
>
@property (nonatomic,strong) WKWebView *webView;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"15093179706";
    self.view.backgroundColor = [UIColor whiteColor];
//    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
//    // 根据需要去设置对应的属性
//    WKWebView *webView = [[WKWebView alloc]initWithFrame:self.view.bounds configuration:config];
//    webView.navigationDelegate = self;
//    [self.view addSubview:webView];
//    NSString *fileUrl = [NSString stringWithFormat:@"download.html"];
//    NSURL *filePath = [[NSBundle mainBundle] URLForResource:fileUrl withExtension:nil];
//    NSURLRequest *request = [NSURLRequest requestWithURL:filePath];
//    [webView loadRequest:request];    // JS调用OC 添加处理脚本
//    [webView.configuration.userContentController addScriptMessageHandler:self name:@"Share"];
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    WKUserContentController *userContentController = [[WKUserContentController alloc] init];
    
    [userContentController addScriptMessageHandler:self name:@"Share"];
    [userContentController addScriptMessageHandler:self name:@"Camera"];
    configuration.userContentController = userContentController;
    
    WKPreferences *preferences = [WKPreferences new];
    preferences.javaScriptCanOpenWindowsAutomatically = YES;
    preferences.minimumFontSize = 40.0;
    configuration.preferences = preferences;
    
    _webView = [[WKWebView alloc]initWithFrame:self.view.bounds configuration:configuration];

    NSString *htmlPath = [[NSBundle mainBundle] pathForResource:@"download" ofType:@"html"];
    NSString *fileURL = [NSString stringWithContentsOfFile:htmlPath encoding:NSUTF8StringEncoding error:nil];
    NSURL *baseURL = [NSURL fileURLWithPath:htmlPath];
    [_webView loadHTMLString:fileURL baseURL:baseURL];
    _webView.UIDelegate = self;
    [self.view addSubview:_webView];
    // Do any additional setup after loading the view.
}
#pragma mark -- WKScriptMessageHandler
-(void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
{
    //JS调用OC方法
    //message.boby就是JS里传过来的参数
//    NSLog(@"body:%@",message.body);
//    
    if ([message.name isEqualToString:@"Share"]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"HTML调用iOS" message:[NSString stringWithFormat:@"%@",message.body] delegate:nil cancelButtonTitle:@"cancle" otherButtonTitles:nil, nil];
        [alertView show];
    } else if ([message.name isEqualToString:@"Camera"]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"HTML调用iOS" message:[NSString stringWithFormat:@"%@",message.body] delegate:nil cancelButtonTitle:@"cancle" otherButtonTitles:nil, nil];
        [alertView show];
    }
}
//- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
//    JSContext *jsContext = [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
//
//// 2. 关联打印异常
//    jsContext.exceptionHandler = ^(JSContext *context, JSValue *exceptionValue) {
//        context.exception = exceptionValue;
//        NSLog(@"异常信息：%@", exceptionValue);
//    };
//     jsContext[@"share"] = ^(NSString *str){
//         UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"HTML调用iOS" message:[NSString stringWithFormat:@"%@",str] delegate:nil cancelButtonTitle:@"cancle" otherButtonTitles:nil, nil];
//         [alertView show];
//     };
//
//
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
